const OSC = require('osc-js');

const options ={
//   receiver: 'udp',         // @param {string} Where messages sent via 'send' method will be delivered to, 'ws' for Websocket clients, 'udp' for udp client
  udpClient: {
     host: '127.0.0.1',
    // host: '192.168.1.58',    // @param {string} Hostname of udp client for messaging
    port: 4715           // @param {number} Port of udp client for messaging
  },
  wsServer: {
    host: 'localhost',    // @param {string} Hostname of WebSocket server
    port: 8080            // @param {number} Port of WebSocket server
  }
}

 const config =  options ;
// const config = { udpClient: { host: '192.168.1.58', port: 4715 } };
// const config = { udpClient: {  port: 4715 } };
const osc = new OSC({ plugin: new OSC.BridgePlugin(config) });

osc.open(); // start a WebSocket server on port 8080

// const osc = new OSC({
//  plugin: new OSC.DatagramPlugin({ send: { port: 1, host: '192.168.1.58' } })
// });
// osc.open();

// const message = new OSC.Message('/test/path', 521.25, 'teststring', 665);
// osc.send(message);
